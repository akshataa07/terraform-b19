# Terraform

---

## What is Terraform?
- Terraform is an open-source infrastructure as code (IaC) tool developed by HashiCorp. 
- uses declarative language (HCL) to express the desired state of infrastructure.
- It supports multiple cloud providers and on-premises environments.
- used for infrastructure provisioning & automates the creation, updating, and management of resources 

---

## Why Terraform?
- Terraform manages state of infrastructure via state file
- Provides :
    - Multi-cloud supports
    - Speed and simplicity
    - Team collaboration
    - Error reduction
    - Disaster management
    - Enhanced security

## Terraform Language 
- HCL (HashiCorp Configuration Language)is Terraform language to define infrastructure configurations.
- Uses blocks for defining resources, variables, providers, etc.
- Supports referencing variables within configurations to include dynamic values.
- Supports single-line (#) and multi-line (/* */) comments for documentation.

